﻿using System;
using System.Data.HashFunction.Blake2;
using System.Globalization;
using System.Linq;
using System.Text;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api")]
    public class EncodingController : ControllerBase
    {
        [Route("convert")]
        [HttpPost]
        public ActionResult ConvertBerTlv()
        {
            var answers = new Answers();

            const string BerTlvEncodedString = "0812561ae6bd42cfb7bee1311a29893508e71c340912e4614f3d9e6c61dccdd5af228051eb4373170A058394b17ac301086ef72ac5367180eb051040d4241e426bbde12bca805f665227e8060234ec07042859ffbc0202b4ed0304bdadb4e00402de60";

            var parsed = BerTlv.Tlv.Parse(BerTlvEncodedString);

            // Task 1 - Extract and order the tag values for the following tag ids by ascending numeric tag id
            var requiredEntities = new[] {1, 3, 4, 5, 6, 10};
            
            var entities = parsed
                .Where(x => requiredEntities.Contains(x.Tag))
                .ToList();

            answers.Task1 = entities
                .OrderBy(x => x.Tag)
                .Select(entry => System.Convert.ToBase64String(entry.Value))
                .ToList();

            // Task 2 - Create an uppercase string with a full stop between each tag value
            answers.Task2 = string
                .Join('.', answers.Task1)
                .ToUpper(CultureInfo.InvariantCulture);

            // Task 3 - Convert the string to a byte array
            answers.Task3 = Encoding.Default.GetBytes(answers.Task2);

            // Task 4 - Hash the byte array using the Blake2b algorithm
            // TODO: Add the salt to the object - this could be the reason the resultant value is incorrect
            var blake2Factory = Blake2BFactory.Instance;
            var blake2Function = blake2Factory.Create();
            answers.Task4 = blake2Function.ComputeHash(answers.Task3).Hash;
            
            // Task 5 - Base64url encode the resulting hash. Starting from the above example you should get: EQ9q1Hjyr_pfSBj1_M2vqAMFH-MYhP2zwhgYfC8u8-g
            answers.Task5 = Convert.ToBase64String(answers.Task4);
            
            return new JsonResult(answers);
        }
    }
}
