﻿using System.Collections.Generic;

namespace Api.Models
{
    public class Answers
    {
        public List<string> Task1 { get; set; }

        public string Task2 { get; set; }

        public byte[] Task3 { get; set; }

        public byte[] Task4 { get; set; }

        public string Task5 { get; set; }
    }
}
