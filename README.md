The test results calculated within an API. All the results come back in one go. This is intentional to enable discussion about how the logic could be extracted.

The blake2b algorithm does not return the correct value. I used a 3rd party NuGet plugin. There are options for a salt which I do not have which would alter the results.

The API has been set up with support for both docker and swagger; this comes out of the box now.

The image is created for you by Visual Studio, however you can build an image from the dockerfile using: *docker build -f "C:\Projects\Learning\CoreTest\Api\Dockerfile" -t coretest .*
NOTE: Update the path to the dockerfile.

Swagger documents the API. This is currently set up to open up in a browser when the solution starts.
